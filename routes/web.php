<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pengurusan-bilik-mesyuarat',function(){
    echo '<h1>UI Pengurusan Bilik Mesyuarat</h1>';
});

Route::get('/tempahan-saya',function(){
    echo '<h1>Tempahan Saya</h1>';
});

Route::get('/senarai-tempahan',function(){
    echo '<h1>Senarai Tempahan</h1>';
});

